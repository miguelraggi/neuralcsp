#pragma once

#include "Misc.hpp"
#include "VectorHelpers.hpp"

using real = double;

template <class Input, int OutputSize>
class NeuralNet
{
public:
    NeuralNet() = default; // later, do the architecture of this.

    std::vector<real> feed_forward(const Input& input) const
    {
        UNUSED(input);

        // TODO(mraggi): modify this
        std::vector<real> output(OutputSize, 1.0);

        return output;
    }

    void giveIO(const Input& input, const std::vector<real>& output)
    {
        cachedIO_.push_back({input, output});
    }

    void train() const
    {
        // I don't know, but send cachedIO_ to neural network to train, do
        // epochs, whatever.
    }

private:
    struct InputOutputPair
    {
        Input input;
        std::vector<real> output;
    };

    std::vector<InputOutputPair> cachedIO_;
};
#pragma once

#include "Misc.hpp"
#include "NeuralNet.hpp"
#include "VectorHelpers.hpp"

inline constexpr int max_state = 40;

class SillyBoardState
{
public:
    constexpr static int OutputSize() { return max_state; }
    constexpr static int MaxTheoreticalEval() { return max_state/2 + 1; }

    std::vector<SillyBoardState> children() const
    {
        std::vector<SillyBoardState> result;
        for (int i = first_valid(); i < max_state; ++i)
        {
            SillyBoardState T = *this; // copy
            T.add(i);
            result.emplace_back(T);
        }
        return result;
    }

    void add(int i) { state_.emplace_back(i); }

    auto state() const { return state_; }

    real evaluate() const { return state_.size(); }

    // false iff children().empty(), but faster...
    // equivalnt to:
    // bool can_move() const { return !children().empty(); }
    bool can_move() const { return first_valid() < max_state; }

    // should be equivalent to *this = pick_random(children())
    void random_move()
    {
        state_.emplace_back(random_int(first_valid(), max_state));
    }

    int first_valid() const 
    {
        if (state_.empty())
            return 0;
        return state_.back() + 2; 
        
    }

private:
    std::vector<int> state_{};
};

inline std::ostream& operator<<(std::ostream& os, const SillyBoardState& T)
{
    os << T.state();
    return os;
}
#include "MonteCarloTreeSearch.hpp"
#include "NeuralNet.hpp"
#include "SillyBoardState.hpp"
#include <iostream>

int main()
{
    MonteCarloTreeSearch<SillyBoardState> M;

    M.search(SillyBoardState{});

    return 0;
}
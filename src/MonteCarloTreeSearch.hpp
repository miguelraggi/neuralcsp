#pragma once

#include "Misc.hpp"
#include "NeuralNet.hpp"
#include "Probability.hpp"
#include "VectorHelpers.hpp"

inline constexpr real INF = 999999999;

template<class BoardState>
real Expected(int n)
{
    return n;
    
//     real M = BoardState::MaxTheoreticalEval();
//     
//     real result = 0;
//     for (long i = 1; i <= M; ++i)
//         result += std::pow(i,n);
//     
//     result /= std::pow(M,n);
//     
//     result = M - result;
//     
//     return result;
}

template <class BoardState>
class MonteCarloTreeSearch
{
public:
    inline static constexpr int simulation_num_times = 1000;
    inline static constexpr int num_times_repeat = 20000;
    inline static constexpr real C = 1.0; // change this when using NN

    MonteCarloTreeSearch() = default;

    void search(const BoardState& state)
    {
        Node root(state);
        for (auto i : NN(num_times_repeat))
        {
            UNUSED(i);
            auto& leaf = select(root);
            expand(leaf);
            
//             {
            real value = simulate(leaf);
            backpropagate(leaf,value);
//             }
            
            for (auto& child : leaf.children())
            {
                real value = simulate(child);
                backpropagate(child, value);
            }
            
        }
//         root.print();
    }

private:
    class Node
    {
    public:
        explicit Node(BoardState state, Node* parent = nullptr)
            : state_(std::move(state)), parent_(parent)
        {}
        Node(const Node&) = delete;
        Node(Node&&) noexcept = default;
        ~Node() = default;
        Node& operator=(const Node&) = delete;
        Node& operator=(Node&&) = delete;

        const BoardState& board_state() const { return state_; }

        int num_visits() const { return num_visits_; }

        real value() const { return value_; }

        real board_evaluation() const { return state_.evaluate(); }

        // returns v + C*sqrt(ln(N)/n) with some nice adjustments
        real confidence(real C) const
        {
            real n = num_visits();
            
            //num visits of parent
            real N = is_root() ? 0 : parent_->num_visits(); 

            auto r = random_real<real>(-epsilon, epsilon);

            return value() + C*std::sqrt(std::log1p(N)/(n + epsilon)) + r;
        }

        bool can_move() const { return state_.can_move(); }

        bool is_leaf() const { return children_.empty(); }
        bool is_root() const { return parent_ == nullptr; }

        void update_stats(real value)
        {
            ++num_visits_;
            replace_by_bigger(value_, value);
        }

        Node* parent() { return parent_; }

        void expand()
        {
            if (!is_leaf())
                return; // don't expand if already expanded!

            for (auto&& b : state_.children())
            {
                children_.emplace_back(std::move(b), this);
            }
        }

        std::vector<Node>& children() { return children_; }

        const BoardState& state() const { return state_; }

        void print(int lvl = 0) const
        {
            if (num_visits() == 0)
                return;
            for (auto i : NN(lvl))
            {
                UNUSED(i);
                std::cout << "    ";
            }

            std::cout << state() << " w/ value " << value()
                      << " and board eval = " << board_evaluation()
                      << " was visited " << num_visits() << " times."
                      << std::endl;
            for (auto& c : children_)
                c.print(lvl + 1);
        }

    private:
        BoardState state_;
        int num_visits_{0};
        real value_{0.0};

        Node* parent_{nullptr};
        std::vector<Node> children_;

        static constexpr real epsilon = 1e-6;
    };

    Node& select(Node& root)
    {
        Node* current = &root;

        while (!current->is_leaf())
        {
            current = &stochastic_select_child(*current);
        }

        return *current;
    }

    void expand(Node& leaf)
    {
        leaf.expand();
    }

    Node& child_with_highest_confidence(Node& node)
    {
        if (node.is_leaf())
            return node;

        Node* s = nullptr;
        real best_confidence = -INF;
        for (auto& child : node.children())
        {
            auto child_conf = child.confidence(10.0);
            if (child_conf > best_confidence)
            {
                s = &child;
                best_confidence = child_conf;
            }
        }

        assert(s != nullptr);
        return *s; // NOLINT: s is assured to be not nullptr!
    }

    Node& stochastic_select_child(Node& node)
    {
        if (node.children().empty())
            return node;
        
        std::vector<real> P(node.children().size());
        for (auto i : indices(P))
        {
            P[i] = node.children()[i].confidence(C);
        }
        normalize(P);
//         std::cout << "Uh, P = " << P << std::endl;
        int i = pick_one_with_probs(P);
//         std::cout << "    so I selected " << i << std::endl;
        return node.children()[i];
    }

    struct avg_and_biggest
    {
        real avg;
        real biggest;
        
//         operator real() const { return (avg+biggest)/2.0; }
        operator real() const { return avg; }
        
    };
    
    avg_and_biggest simulate(Node& node)
    {
        avg_and_biggest result = {0.0,0.0};

        for (auto i : NN(simulation_num_times))
        {
            UNUSED(i);
            
            BoardState B = node.state();

            while (B.can_move())
            {
                B.random_move();
            }
            auto b_eval = B.evaluate();
            
            result.avg += b_eval;
            
            replace_by_bigger(result.biggest,b_eval);
            
            if (global_best_eval_ < b_eval)
            {
                global_best_eval_ = b_eval;
                global_best_ = B;
                std::cout << "New best found: |" << B << "| with value " << global_best_eval_ << std::endl;
            }
            
        }
        result.avg /= simulation_num_times;

        return result;
    }

    void backpropagate(Node& leaf, real value)
    {
        Node* s = &leaf;
        while (!s->is_root())
        {
            s->update_stats(value);
            s = s->parent();
        }
        s->update_stats(value); //root
    }

    NeuralNet<Node, BoardState::OutputSize()> NN_;
    BoardState global_best_;
    real global_best_eval_ = -INF;
};
